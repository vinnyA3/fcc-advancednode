'use strict';

const express = require('express');
const bodyParser = require('body-parser');
const fccTesting = require('./freeCodeCamp/fcctesting.js');
const port = process.env.PORT || 3000;

const app = express();

fccTesting(app); //For FCC testing purposes
app.use('/public', express.static(process.cwd() + '/public'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
// set template engine
app.set('view engine', 'pug');

app.route('/').get((req, res) => {
  // res.sendFile(process.cwd() + '/views/index.html');
  res.render('pug/index.pug');
});

app.listen(port, () => {
  console.log('Listening on port ' + port);
});
